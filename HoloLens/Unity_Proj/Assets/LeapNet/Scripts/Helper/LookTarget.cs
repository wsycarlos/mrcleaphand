﻿using UnityEngine;
using System.Collections;

public class LookTarget : MonoBehaviour
{
    public Transform target;

    void Start()
    {
        transform.LookAt(target);
    }
}