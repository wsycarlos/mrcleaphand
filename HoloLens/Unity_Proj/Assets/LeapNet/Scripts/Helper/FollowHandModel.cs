﻿using UnityEngine;
using System.Collections;

public class FollowHandModel : MonoBehaviour
{
    public LeapSharingHand handModel;

    public Vector3 direction;

    private SharingHand hand;

    void Start()
    {
        hand = handModel.GetLeapHand();
        if (hand != null)
        {
            transform.localPosition = hand.PalmPosition;
            transform.localRotation = hand.Rotation;
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }

    void Update()
    {
        hand = handModel.GetLeapHand();
        if (hand != null)
        {
            transform.localPosition = hand.PalmPosition;
            transform.localRotation = hand.Rotation;
        }
    }
}
