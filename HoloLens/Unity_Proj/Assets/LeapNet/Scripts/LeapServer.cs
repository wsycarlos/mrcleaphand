﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using HoloToolkit.Sharing;
using HoloToolkit.Unity;
using System;

public class LeapServer : Singleton<LeapServer>
{
    private bool showGesture = false;

    private float size = 2f;

    public void Start()
    {
        DisplayHand();
        LeapSharingData.Instance.MessageHandlers[LeapSharingData.TestMessageID.HeadHand] = MessageDispatcher;
    }

    void OnEnable()
    {
        Messenger.AddListener("SwitchToHand", SwitchToHand);
        Messenger.AddListener("SwitchToGes", SwitchToGes);
        Messenger.AddListener("LargerHand", LargerHand);
        Messenger.AddListener("SmallerHand", SmallerHand);
    }

    void OnDisable()
    {
        Messenger.RemoveListener("SwitchToHand", SwitchToHand);
        Messenger.RemoveListener("SwitchToGes", SwitchToGes);
        Messenger.RemoveListener("LargerHand", LargerHand);
        Messenger.RemoveListener("SmallerHand", SmallerHand);
    }

    private void MessageDispatcher(NetworkInMessage msg)
    {
        // Parse the message
        long userID = msg.ReadInt64();

        LeapSharingData.MessageType msgType = (LeapSharingData.MessageType)msg.ReadByte();
        
        if(msgType == LeapSharingData.MessageType.BEGIN_L_HAND)
        {
            SharingHand sh = LeapSharingData.Instance.ReadHand(msg);
            leftHand.BeginHand(sh);
        }
        else if(msgType == LeapSharingData.MessageType.BEGIN_R_HAND)
        {
            SharingHand sh = LeapSharingData.Instance.ReadHand(msg);
            rightHand.BeginHand(sh);
        }
        else if (msgType == LeapSharingData.MessageType.FINISH_L_HAND)
        {
            Messenger.Broadcast("EndGesture_0");
            leftHand.FinishHand();
        }
        else if (msgType == LeapSharingData.MessageType.FINISH_R_HAND)
        {
            Messenger.Broadcast("EndGesture_1");
            rightHand.FinishHand();
        }
        else if (msgType == LeapSharingData.MessageType.SMALLER_HAND)
        {
            Messenger.Broadcast("SmallerHand");
        }
        else if (msgType == LeapSharingData.MessageType.LARGER_HAND)
        {
            Messenger.Broadcast("LargerHand");
        }
        else if (msgType == LeapSharingData.MessageType.SET_L_HAND)
        {
            SharingHand sh = LeapSharingData.Instance.ReadHand(msg);
            leftHand.SetLeapHand(sh);
        }
        else if (msgType == LeapSharingData.MessageType.SET_R_HAND)
        {
            SharingHand sh = LeapSharingData.Instance.ReadHand(msg);
            rightHand.SetLeapHand(sh);
        }
        else if (msgType == LeapSharingData.MessageType.SWITCH_TO_GES)
        {
            Messenger.Broadcast("SwitchToGes");
        }
        else if (msgType == LeapSharingData.MessageType.SWITCH_TO_HAND)
        {
            Messenger.Broadcast("SwitchToHand");
        }
        else if (msgType == LeapSharingData.MessageType.RESET_POSITION)
        {
            Messenger.Broadcast("ResetPosition");
            Messenger.Broadcast("ResetHand");
        }
        else if (msgType == LeapSharingData.MessageType.LEFT_G_1)
        {
            Messenger.Broadcast<int>("Gesture_0", 1);
        }
        else if (msgType == LeapSharingData.MessageType.LEFT_G_2)
        {
            Messenger.Broadcast<int>("Gesture_0", 2);
        }
        else if (msgType == LeapSharingData.MessageType.LEFT_G_3)
        {
            Messenger.Broadcast<int>("Gesture_0", 3);
        }
        else if (msgType == LeapSharingData.MessageType.LEFT_G_4)
        {
            Messenger.Broadcast<int>("Gesture_0", 4);
        }
        else if (msgType == LeapSharingData.MessageType.RIGHT_G_1)
        {
            Messenger.Broadcast<int>("Gesture_1", 1);
        }
        else if (msgType == LeapSharingData.MessageType.RIGHT_G_2)
        {
            Messenger.Broadcast<int>("Gesture_1", 2);
        }
        else if (msgType == LeapSharingData.MessageType.RIGHT_G_3)
        {
            Messenger.Broadcast<int>("Gesture_1", 3);
        }
        else if (msgType == LeapSharingData.MessageType.RIGHT_G_4)
        {
            Messenger.Broadcast<int>("Gesture_1", 4);
        }
        else
        {
            Debug.LogError("Wrong Message Type");
        }
    }

    public void SwitchToHand()
    {
        showGesture = false;
        DisplayHand();
    }

    public void SwitchToGes()
    {
        showGesture = true;
        DisplayHand();
    }

    public void LargerHand()
    {
        size = Mathf.Clamp(size + 0.25f, 0.5f, 6f);
        DisplayHand();
    }

    public void SmallerHand()
    {
        size = Mathf.Clamp(size - 0.25f, 0.5f, 6f);
        DisplayHand();
    }

    private void DisplayHand()
    {
        leftHand.gameObject.SetActive(!showGesture);
        rightHand.gameObject.SetActive(!showGesture);
        leftGesture.gameObject.SetActive(showGesture);
        rightGesture.gameObject.SetActive(showGesture);
        leftHand.transform.localScale = new Vector3(size, size, size);
        rightHand.transform.localScale = new Vector3(size, size, size);
        leftGesture.transform.localScale = new Vector3(size, size, size);
        rightGesture.transform.localScale = new Vector3(size, size, size);
    }

    public LeapSharingHand leftHand;
    public LeapSharingHand rightHand;

    public Transform leftGesture;
    public Transform rightGesture;

}