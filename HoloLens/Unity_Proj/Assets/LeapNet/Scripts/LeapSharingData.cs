﻿using System;
using System.Collections.Generic;
using HoloToolkit.Unity;
using HoloToolkit.Sharing;
using UnityEngine;

public class LeapSharingData : Singleton<LeapSharingData>
{
    public enum MessageType : byte
    {
        BEGIN_L_HAND,
        SET_L_HAND,
        FINISH_L_HAND,
        BEGIN_R_HAND,
        SET_R_HAND,
        FINISH_R_HAND,
        LEFT_G_1,
        LEFT_G_2,
        LEFT_G_3,
        LEFT_G_4,
        RIGHT_G_1,
        RIGHT_G_2,
        RIGHT_G_3,
        RIGHT_G_4,
        RESET_POSITION,
        SWITCH_TO_HAND,
        SWITCH_TO_GES,
        SMALLER_HAND,
        LARGER_HAND,
    }

    /// <summary>
    /// Message enum containing our information bytes to share.
    /// The first message type has to start with UserMessageIDStart
    /// so as not to conflict with HoloToolkit internal messages.
    /// </summary>
    public enum TestMessageID : byte
    {
        HeadHand = MessageID.UserMessageIDStart,
        Max
    }

    public enum UserMessageChannels
    {
        Anchors = MessageChannel.UserMessageChannelStart
    }

    /// <summary>
    /// Cache the local user's ID to use when sending messages
    /// </summary>
    public long LocalUserID
    {
        get; set;
    }

    public delegate void MessageCallback(NetworkInMessage msg);
    private Dictionary<TestMessageID, MessageCallback> messageHandlers = new Dictionary<TestMessageID, MessageCallback>();
    public Dictionary<TestMessageID, MessageCallback> MessageHandlers
    {
        get
        {
            return messageHandlers;
        }
    }

    /// <summary>
    /// Helper object that we use to route incoming message callbacks to the member
    /// functions of this class
    /// </summary>
    private NetworkConnectionAdapter connectionAdapter;

    /// <summary>
    /// Cache the connection object for the sharing service
    /// </summary>
    private NetworkConnection serverConnection;

    private void Start()
    {
        // SharingStage should be valid at this point, but we may not be connected.
        if (SharingStage.Instance.IsConnected)
        {
            Connected();
        }
        else
        {
            SharingStage.Instance.SharingManagerConnected += Connected;
        }
    }

    private void Connected(object sender = null, EventArgs e = null)
    {
        SharingStage.Instance.SharingManagerConnected -= Connected;
        InitializeMessageHandlers();
    }

    private void InitializeMessageHandlers()
    {
        SharingStage sharingStage = SharingStage.Instance;

        if (sharingStage == null)
        {
            Debug.Log("Cannot Initialize CustomMessages. No SharingStage instance found.");
            return;
        }

        serverConnection = sharingStage.Manager.GetServerConnection();
        if (serverConnection == null)
        {
            Debug.Log("Cannot initialize CustomMessages. Cannot get a server connection.");
            return;
        }

        connectionAdapter = new NetworkConnectionAdapter();
        connectionAdapter.MessageReceivedCallback += OnMessageReceived;

        // Cache the local user ID
        LocalUserID = SharingStage.Instance.Manager.GetLocalUser().GetID();

        for (byte index = (byte)TestMessageID.HeadHand; index < (byte)TestMessageID.Max; index++)
        {
            if (MessageHandlers.ContainsKey((TestMessageID)index) == false)
            {
                MessageHandlers.Add((TestMessageID)index, null);
            }

            serverConnection.AddListener(index, connectionAdapter);
        }
    }

    private NetworkOutMessage CreateMessage(byte messageType)
    {
        NetworkOutMessage msg = serverConnection.CreateMessage(messageType);
        msg.Write(messageType);
        // Add the local userID so that the remote clients know whose message they are receiving
        msg.Write(LocalUserID);
        return msg;
    }

    public void SendMessage(MessageType type, SharingHand sh)
    {
        // If we are connected to a session, broadcast our head info
        if (serverConnection != null && serverConnection.IsConnected())
        {
            // Create an outgoing network message to contain all the info we want to send
            NetworkOutMessage msg = CreateMessage((byte)TestMessageID.HeadHand);

            msg.Write(Convert.ToByte(type));

            AppendHand(msg, sh);

            // Send the message as a broadcast, which will cause the server to forward it to all other users in the session.
            serverConnection.Broadcast(
                msg,
                MessagePriority.Immediate,
                MessageReliability.UnreliableSequenced,
                MessageChannel.Avatar);
        }
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        if (serverConnection != null)
        {
            for (byte index = (byte)TestMessageID.HeadHand; index < (byte)TestMessageID.Max; index++)
            {
                serverConnection.RemoveListener(index, connectionAdapter);
            }
            connectionAdapter.MessageReceivedCallback -= OnMessageReceived;
        }
    }

    private void OnMessageReceived(NetworkConnection connection, NetworkInMessage msg)
    {
        byte messageType = msg.ReadByte();
        MessageCallback messageHandler = MessageHandlers[(TestMessageID)messageType];
        if (messageHandler != null)
        {
            messageHandler(msg);
        }
    }

    #region HelperFunctionsForWriting
    
    private void AppendHand(NetworkOutMessage msg, SharingHand sh)
    {
        if (sh != null)
        {
            if (sh.Fingers == null || sh.Fingers.Count == 0)
            {
                msg.Write((Int32)0);
            }
            else
            {
                msg.Write(sh.Fingers.Count);
                foreach (SharingFinger f in sh.Fingers)
                {
                    AppendFinger(msg, f);
                }
            }
            msg.Write(Convert.ToByte(sh.IsLeft));
            AppendVector3(msg, sh.PalmPosition);
            AppendVector3(msg, sh.PalmNormal);
            AppendQuaternion(msg, sh.Rotation);
            AppendVector3(msg, sh.XBasis);
        }
    }

    private void AppendFinger(NetworkOutMessage msg, SharingFinger sf)
    {
        if (sf != null)
        {
            if (sf.bones == null || sf.bones.Length == 0)
            {
                msg.Write((Int32)0);
            }
            else
            {
                msg.Write(sf.bones.Length);
                for (int i = 0; i < sf.bones.Length; i++)
                {
                    AppendBone(msg, sf.bones[i]);
                }
            }
            msg.Write(sf.Type);
        }
    }

    private void AppendBone(NetworkOutMessage msg, SharingBone sb)
    {
        if (sb != null)
        {
            AppendVector3(msg, sb.NextJoint);
            msg.Write(sb.Type);
        }
    }

    //private void AppendTransform(NetworkOutMessage msg, Vector3 position, Quaternion rotation)
    //{
    //    AppendVector3(msg, position);
    //    AppendQuaternion(msg, rotation);
    //}

    private void AppendVector3(NetworkOutMessage msg, Vector3 vector)
    {
        msg.Write(vector.x);
        msg.Write(vector.y);
        msg.Write(vector.z);
    }

    private void AppendQuaternion(NetworkOutMessage msg, Quaternion rotation)
    {
        msg.Write(rotation.x);
        msg.Write(rotation.y);
        msg.Write(rotation.z);
        msg.Write(rotation.w);
    }

    #endregion

    #region HelperFunctionsForReading

    public SharingHand ReadHand(NetworkInMessage msg)
    {
        SharingHand h = new SharingHand();
        int count = msg.ReadInt32();
        h.Fingers = new List<SharingFinger>();
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                SharingFinger f = ReadFinger(msg);
                h.Fingers.Add(f);
            }
        }
        h.IsLeft = Convert.ToBoolean(msg.ReadByte());
        h.PalmPosition = ReadVector3(msg);
        h.PalmNormal = ReadVector3(msg);
        h.Rotation = ReadQuaternion(msg);
        h.XBasis = ReadVector3(msg);

        return h;
    }

    public SharingFinger ReadFinger(NetworkInMessage msg)
    {
        SharingFinger f = new SharingFinger();
        int count = msg.ReadInt32();
        if (count > 0)
        {
            f.bones = new SharingBone[count];
            for (int i = 0; i < f.bones.Length; i++)
            {
                f.bones[i] = ReadBone(msg);
            }
        }
        f.Type = msg.ReadInt32();
        return f;
    }

    public SharingBone ReadBone(NetworkInMessage msg)
    {
        SharingBone b = new SharingBone();
        b.NextJoint = ReadVector3(msg);
        b.Type = msg.ReadInt32();
        return b;
    }

    public Vector3 ReadVector3(NetworkInMessage msg)
    {
        return new Vector3(msg.ReadFloat(), msg.ReadFloat(), msg.ReadFloat());
    }

    public Quaternion ReadQuaternion(NetworkInMessage msg)
    {
        return new Quaternion(msg.ReadFloat(), msg.ReadFloat(), msg.ReadFloat(), msg.ReadFloat());
    }

    #endregion
}


public class SharingHand
{
    public List<SharingFinger> Fingers;
    public bool IsLeft;
    public Vector3 PalmPosition;
    public Vector3 PalmNormal;
    public Quaternion Rotation;
    public Vector3 XBasis;
}

public class SharingFinger
{
    public SharingBone[] bones;
    public int Type;
    
    public SharingBone Bone(int bonelx)
    {
        for (int i = 0; i < bones.Length; i++)
        {
            if (bones[i].Type == bonelx)
            {
                return bones[i];
            }
        }
        return null;
    }
}

public class SharingBone
{
    public Vector3 NextJoint;
    public int Type;
}
