﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System;
using System.Text;
using System.IO;

public class XmlData
{
    public static Object DeserializeFromFile(string filePath, Type type)
    {
        return DeserializeFromString(File.ReadAllText(filePath), type, null, null);
    }

    public static Object DeserializeFromFile(string filePath, Type type, XmlRootAttribute root, XmlAttributeOverrides overrides)
    {
        if (File.Exists(filePath))
        {
            return DeserializeFromString(File.ReadAllText(filePath), type, root, overrides);
        }
        else
        {
            Console.WriteLine("File not found");
        }

        return null;
    }

    public static Object DeserializeFromString(string xmlString, Type type)
    {
        return DeserializeFromString(xmlString, type, null, null);
    }

    public static Object DeserializeFromString(string xmlString, Type type, XmlRootAttribute root, XmlAttributeOverrides overrides)
    {
        if (!string.IsNullOrEmpty(xmlString))
        {
            XmlSerializer serializer = new XmlSerializer(type, overrides, null, root, string.Empty);
            UTF8Encoding encoding = new UTF8Encoding();
            MemoryStream stream = new MemoryStream(encoding.GetBytes(xmlString));
            return serializer.Deserialize(stream);
        }
        else
        {
            Console.WriteLine("Xml string is empty");
        }

        return null;
    }

    public static void SerializeToFile(string filePath, Object obj)
    {
        SerializeToFile(filePath, obj, null, null);
    }

    public static void SerializeToFile(string filePath, Object obj, XmlRootAttribute root, XmlAttributeOverrides overrides)
    {
        XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType(), overrides, null, root, string.Empty);

        StreamWriter streamWriter = new StreamWriter(filePath, false, new UTF8Encoding());
        xmlSerializer.Serialize(streamWriter, obj);

        streamWriter.Close();
    }
}