﻿using UnityEngine;
using System.Collections;
using Leap.Unity;

public class FollowHandModel : MonoBehaviour
{
    public IHandModel handModel;

    public Vector3 direction;

    private Leap.Hand _hand = null;
    private Leap.Hand hand
    {
        get
        {
            if (_hand == null)
            {
                if (handModel != null)
                {
                    _hand = handModel.GetLeapHand();
                }
            }
            return _hand;
        }
    }

    void Update()
    {
        if (hand != null)
        {
            transform.position = hand.PalmPosition.ToVector3();
            transform.rotation = hand.Basis.CalculateRotation();
        }
    }
}
