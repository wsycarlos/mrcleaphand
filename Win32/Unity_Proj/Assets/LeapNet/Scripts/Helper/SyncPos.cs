﻿using UnityEngine;
using System.Collections;

public class SyncPos : MonoBehaviour
{
    public string targetGameObject;

    private Transform pos = null;
    // Use this for initialization
    void Start()
    {
        GameObject target = GameObject.Find(targetGameObject);
        if (target != null)
        {
            pos = target.transform;
            Sync();
        }
    }

    void OnEnable()
    {
        Messenger.AddListener("ResetPosition", Sync);
    }

    void OnDisable()
    {
        Messenger.RemoveListener("ResetPosition", Sync);
    }

    // Update is called once per frame
    public void Sync()
    {
        if (pos != null)
        {
            transform.position = pos.position;
            transform.rotation = pos.rotation;
        }
        //transform.localScale = pos.localScale;
    }
}
