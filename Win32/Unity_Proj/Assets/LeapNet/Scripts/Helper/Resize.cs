﻿using UnityEngine;
using System.Collections;

public class Resize : MonoBehaviour
{
    int pW = 1280;
    int pH = 720;

    float ratio;

    int lastW=-1;
    int lastH=-1;

    void Start()
    {
        ratio = (float)pW / (float)pH;
    }

    void Update()
    {
        int w = Screen.width;
        int h = Screen.height;

        if(lastW != w || lastH != h)
        {
            Reset(w, h);
            lastW = w;
            lastH = h;
        }
    }

    // Update is called once per frame
    void Reset(int w, int h)
    {

        float fW = (float)w / (float)pW;
        float fH = (float)h / (float)pH;

        if(fW >= fH)
        {
            float rw = ratio * 2;
            transform.localScale = new Vector3(rw, 2, 1);
        }
        else
        {
            float ah = (float)w / ratio;
            float bh = ah / h;
            float rw = (float)w / (float)h * 2;
            transform.localScale = new Vector3(rw, bh * 2, 1);
        }
    }
}