﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Xml.Serialization;

public class Bootloader : MonoBehaviour
{
    private static Bootloader mInstance;
    string sharingServer = "192.168.137.1";
    string hololens = "192.168.137.244";
    bool connected = false;

    public Texture g1;
    public Texture g2;
    public Texture g3;
    public Texture g4;

    public static string SharingServer
    {
        get
        {
            if (mInstance != null)
            {
                return mInstance.sharingServer;
            }
            return null;
        }
    }

    public static string HoloLens
    {
        get
        {
            if (mInstance != null)
            {
                return mInstance.hololens;
            }
            return null;
        }
    }

    void OnEnable()
    {
        if (mInstance == null)
        {
            mInstance = this;
        }
    }

    void OnDisable()
    {
        if (mInstance != null)
        {
            mInstance = null;
        }
    }
    
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        //Address old = new Address();
        //old.server = sharingServer;
        //old.hololens = hololens;
        //XmlData.SerializeToFile(Application.dataPath + "/address.xml", old);
        Address adr = XmlData.DeserializeFromFile(Application.dataPath + "/address.xml", typeof(Address)) as Address;
        sharingServer = adr.server;
        hololens = adr.hololens;
    }

    void OnGUI()
    {
        if (!connected)
        {
            //GUIStyle tstyle = new GUIStyle(GUI.skin.textField);
            //GUIStyle bstyle = new GUIStyle(GUI.skin.button);
            //tstyle.fontSize = 36;
            //bstyle.fontSize = 36;
            //GUILayout.BeginVertical();
            //GUILayout.BeginHorizontal();
            //GUILayout.Label("Sharing Server Address:");
            //sharingServer = GUILayout.TextField(sharingServer, tstyle, GUILayout.Width(300), GUILayout.Height(120));
            //GUILayout.EndHorizontal();
            //GUILayout.BeginHorizontal();
            //GUILayout.Label("HoloLens Address:");
            //hololens = GUILayout.TextField(hololens, tstyle, GUILayout.Width(300), GUILayout.Height(120));
            //GUILayout.EndHorizontal();
            //if (GUILayout.Button("Connect", bstyle, GUILayout.Width(400), GUILayout.Height(200)))
            //{
            //    SceneManager.LoadScene("leapnet");
            //    connected = true;
            //}
            //GUILayout.EndVertical();
            SceneManager.LoadScene("leapnet");
            connected = true;
        }
        else
        {
            GUIStyle bstyle = new GUIStyle(GUI.skin.button);
            bstyle.fontSize = 36;
            if (GUI.Button(new Rect(Screen.width / 2 - 170, 20, 330, 50), "(R)eset", bstyle) || Input.GetKeyUp(KeyCode.R))
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.RESET_POSITION, null);
                //player.SendCommand("ResetPosition");
            }
            if (GUI.Button(new Rect(Screen.width / 2 - 170, 80, 160, 50), "(H)and", bstyle) || Input.GetKeyUp(KeyCode.H))
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.SWITCH_TO_HAND, null);
                //player.SendCommand("SwitchToHand");
            }
            if (GUI.Button(new Rect(Screen.width / 2 + 10, 80, 160, 50), "(G)esture", bstyle) || Input.GetKeyUp(KeyCode.G))
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.SWITCH_TO_GES, null);
                //player.SendCommand("SwitchToGes");
            }
            if (GUI.Button(new Rect(Screen.width / 2 - 170, 140, 160, 50), "(S)maller", bstyle) || Input.GetKeyUp(KeyCode.S))
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.SMALLER_HAND, null);
                //player.SendCommand("SmallerHand");
            }
            if (GUI.Button(new Rect(Screen.width / 2 + 10, 140, 160, 50), "(L)arger", bstyle) || Input.GetKeyUp(KeyCode.L))
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.LARGER_HAND, null);
                //player.SendCommand("LargerHand");
            }
            //
            if (GUI.Button(new Rect(Screen.width / 2 - 530, 20, 170, 170), g1) || Input.GetKeyUp(KeyCode.Alpha1))
            {
                Messenger.Broadcast<int>("GestureRec", 1);
            }
            if (GUI.Button(new Rect(Screen.width / 2 - 350, 20, 170, 170), g2) || Input.GetKeyUp(KeyCode.Alpha2))
            {
                Messenger.Broadcast<int>("GestureRec", 2);
            }
            if (GUI.Button(new Rect(Screen.width / 2 + 180, 20, 170, 170), g3) || Input.GetKeyUp(KeyCode.Alpha3))
            {
                Messenger.Broadcast<int>("GestureRec", 3);
            }
            if (GUI.Button(new Rect(Screen.width / 2 + 360, 20, 170, 170), g4) || Input.GetKeyUp(KeyCode.Alpha4))
            {
                Messenger.Broadcast<int>("GestureRec", 4);
            }
        }
    }
}

[System.Serializable]
public class Address
{
    [XmlAttribute("SharingServerAddress")]
    public string server;

    [XmlAttribute("HoloLensAddress")]
    public string hololens;
}