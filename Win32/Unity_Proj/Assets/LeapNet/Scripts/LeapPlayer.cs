﻿//using UnityEngine;
//using System.Collections;
//using UnityEngine.Networking;
//using System.Collections.Generic;
//using System.Linq;
//using System.IO;

//[NetworkSettings(sendInterval = 0.01f)]
//public class LeapPlayer : NetworkBehaviour
//{
//    public void Start()
//    {
//        Debug.Log("Leap Player Start");
//    }
    
//    public void BeginHand(int hand, byte[] arrHand)
//    {
//        try
//        {
//            Debug.Log("Begin Hand:" + hand);

//            CmdBeginHand(hand, arrHand);
//        }
//        catch (System.Exception e)
//        {
//            Debug.LogException(e);
//        }
//    }

//    public void Gesture(int hand, int gesture)
//    {
//        try
//        {
//            Debug.Log("Gesture:" + hand + " " + gesture);

//            CmdGesture(hand, gesture);
//        }
//        catch (System.Exception e)
//        {
//            Debug.LogException(e);
//        }
//    }

//    public void FinishHand(int hand)
//    {
//        try
//        {
//            Debug.Log("Finish Hand:" + hand);

//            CmdFinishHand(hand);
//        }
//        catch (System.Exception e)
//        {
//            Debug.LogException(e);
//        }
//    }

//    public void SetLeapHand(int hand, byte[] arrHand)
//    {
//        try
//        {
//            CmdSetLeapHand(hand, arrHand);
//        }
//        catch (System.Exception e)
//        {
//            Debug.LogException(e);
//        }
//    }

//    public void SendCommand(string cmd)
//    {
//        try
//        {
//            CmdCommand(cmd);
//        }
//        catch (System.Exception e)
//        {
//            Debug.LogException(e);
//        }
//    }

//    [Command(channel = 0)]
//    public void CmdBeginHand(int hand, byte[] arrHand) { }

//    [Command(channel = 0)]
//    public void CmdFinishHand(int hand) { }

//    [Command(channel = 0)]
//    public void CmdGesture(int hand, int gesture) { }

//    [Command(channel = 1)]
//    public void CmdSetLeapHand(int hand, byte[] arrHand) { }

//    [Command(channel = 0)]
//    public void CmdCommand(string cmd) { }
//}