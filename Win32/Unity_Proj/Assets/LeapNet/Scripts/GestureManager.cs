﻿using UnityEngine;
using System.Collections;
using Leap.Unity;

public class GestureManager : MonoBehaviour
{
    //LeapPlayer _player = null;
    //private LeapPlayer player
    //{
    //    get
    //    {
    //        if (_player == null)
    //        {
    //            _player = FindObjectOfType<LeapPlayer>();
    //        }
    //        return _player;
    //    }
    //}

    public IHandModel handModel;

    GameObject model;
    int globalGes = 1;

    public GameObject[] gestureModels;

    void OnEnable()
    {
        handModel.OnFinish += EndHand;
        Messenger.AddListener<int>("GestureRec", GestureRec);
    }

    void OnDisable()
    {
        handModel.OnFinish -= EndHand;
        Messenger.RemoveListener<int>("GestureRec", GestureRec);
    }

    public void EndHand()
    {
        DHand();
    }

    private void DHand()
    {
        if (model != null)
        {
            Destroy(model);
        }
    }

    public void GestureRec(int ges)
    {
        globalGes = ges;
        GestureHere();
    }

    public void GestureHere()
    {
        DHand();
        model = Instantiate(gestureModels[globalGes - 1]) as GameObject;
        FollowHandModel fhm = model.AddComponent<FollowHandModel>();
        fhm.handModel = handModel;
        Vector3 v1 = new Vector3(0, (globalGes < 3) ? (-1) : (1), 0);
        Vector3 v2 = new Vector3(0, 0, 1);
        Vector3 v3 = Vector3.Cross(v1, v2);
        v3.Normalize();
        fhm.direction = v1 + v2 + v3;
        fhm.direction.Normalize();
        model.SetActive(true);
        if(handModel.Handedness== Chirality.Left)
        {
            if (globalGes == 1)
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.LEFT_G_1, null);
            }
            else if(globalGes == 2)
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.LEFT_G_2, null);
            }
            else if (globalGes == 3)
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.LEFT_G_3, null);
            }
            else
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.LEFT_G_4, null);
            }
        }
        else
        {
            if (globalGes == 1)
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.RIGHT_G_1, null);
            }
            else if (globalGes == 2)
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.RIGHT_G_2, null);
            }
            else if (globalGes == 3)
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.RIGHT_G_3, null);
            }
            else
            {
                LeapSharingData.Instance.SendMessage(LeapSharingData.MessageType.RIGHT_G_4, null);
            }
        }
    }
}