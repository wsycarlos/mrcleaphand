﻿//using UnityEngine;
//using System.Collections;
//using UnityEngine.Networking;
//using MixedRemoteViewCompositor;

//namespace LeapNet
//{
//    public class LeapNet : NetworkManager
//    {
//        void Start()
//        {
//            Application.targetFrameRate = 30;
//            Application.RequestUserAuthorization(UserAuthorization.Microphone);
//            networkAddress = Bootloader.IpAddress;
//            StartClient();
//            MrvcManager mrvc = FindObjectOfType<MrvcManager>();
//            if (mrvc != null)
//            {
//                mrvc.SetConnectorAddress(networkAddress);
//                mrvc.StartPlayback();
//            }
//        }

//        public override void OnClientConnect(NetworkConnection conn)
//        {
//            ClientScene.AddPlayer(client.connection, 0);
//            conn.SetChannelOption(2, ChannelOption.MaxPendingBuffers, 128);
//            Debug.Log("Server Connected!");
//        }

//        public override void OnClientDisconnect(NetworkConnection conn)
//        {
//            LeapPlayer _p = FindObjectOfType<LeapPlayer>();
//            if(_p != null)
//            {
//                Destroy(_p.gameObject);
//            }
//            Debug.Log("Server Disconnected!");
//            StartCoroutine("Reset");
//        }

//        IEnumerator Reset()
//        {
//            StopClient();
//            yield return new WaitForSeconds(1f);
//            StartClient();
//        }

//        public override void OnClientError(NetworkConnection conn, int errorCode)
//        {
//            Debug.LogError("Connect Error:" + errorCode);
//            StartCoroutine("Reset");
//        }

//        public override void OnStartServer()
//        {
//        }

//        public override void OnServerConnect(NetworkConnection conn)
//        {
//            Debug.Log("Client Connect to Server!");
//        }

//        public override void OnServerDisconnect(NetworkConnection conn)
//        {
//            base.OnServerDisconnect(conn);
//            Debug.Log("Client Disconnect from Server!");
//        }

//        public override void OnStartClient(NetworkClient client)
//        {
//            Debug.Log("Start Client!");
//        }

//        public override void OnStopClient()
//        {
//            Debug.Log("Stop Client!");
//        }

//        public override void OnStopServer()
//        {
//            Debug.Log("Stop Server!");
//        }
//    }
//}