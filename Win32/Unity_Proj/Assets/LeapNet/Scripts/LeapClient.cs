﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MixedRemoteViewCompositor;
using HoloToolkit.Sharing;

public class LeapClient : MonoBehaviour
{
    void Start()
    {
        Application.RequestUserAuthorization(UserAuthorization.Microphone);
        SharingStage.Instance.ServerAddress = Bootloader.SharingServer;
        SharingStage.Instance.ConnectToServer();
        MrvcManager mrvc = FindObjectOfType<MrvcManager>();
        if (mrvc != null)
        {
            mrvc.SetConnectorAddress(Bootloader.HoloLens);
            mrvc.StartPlayback();
        }
    }
}
